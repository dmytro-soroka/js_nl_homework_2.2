/* 
    TASK 1 (Рекурсия)

    Напишите функцию, которая принимает на вход положительное число n и выводит все натуральные числа
    от n до 1 включительно в порядке убывания с помощью рекурсии

    Example: recurs(7) => 7,6,5,4,3,2,1
*/


function getNum(n) {
    console.log(n);
    if (n > 1) {
        getNum(n - 1);
    };
}

const recursResult = getNum(7);

console.log(recursResult);

/* 
    TASK 2 (Рекурсия)

    Напишите функцию которая принимает на вход 2 параметра : 'x' и 'y', и выводит все числа от x до y включительно в порядке убывания. 
    Если же x меньше y, то в порядке возрастания. Задачу необходимо выполнить с помощью рекурсии

    Example: recurs(20, 10) => 20,19,18 ......,10
    Example: recurs(30, 50) => 30,31,32 ......,50
*/

function recurs(x, y) {
    console.log(x);
    if (x > y && x != y) {
        recurs(x - 1, y);
    } else if (x < y && x != y) {
        recurs(x + 1, y)
    }
}

console.log(recurs(20, 10))



/* 
    TASK 3 (SetInterval)

    Необходимо сделать часы с помощью setInterval и объекта Date. 
    Они должны быть в формате ЧАСЫ:МИНУТЫ:СЕКУНДЫ.
    И если у нас сейчас 1 час ночи, то должно показываться 01, а не просто 1. 
    Если сейчас время один час пять минут, 
    то у вас должно выводиться 01:05, а не 1:5
*/

function clock() {
    function setNormaltime(num) {
        if (num <= 9) {
            return num = '0' + num;
        } else {
            return num;
        }
    };

    let date = new Date();
    console.log(`${setNormaltime(date.getHours())}:${setNormaltime(date.getMinutes())}:${setNormaltime(date.getSeconds())}`);
}

// setInterval(clock, 1000);

/* 
    TASK 4 (Closure)

    Создайте замыкание: 
    функция makePassword получает пароль в параметре и возвращает внутреннюю функцию,
    которая принимает введенную строку и возвращает булево значение true, если введенная строка совпадает с паролем,
    и false – если не совпадает.

    function makePassword(password) {
        return       ваш код     {
            return (tryPassword === password);
        };
    }
    
    ваш код
*/

function makePassword(password) {
    return function (tryPassword) {
        return (tryPassword === password);
    };
}

const resultPass = makePassword('1234');

console.log(resultPass('12342'))



/*
    TASK 5 (Closure)

    Создайте замыкание:
    функция addition получает число n и возвращает внутреннюю функцию. 
    Эта функция также получает число, прибавляет его к n и возвращает результат

    function addition(n) {
        return     ваш код     {
            return     ваш код    ;
        };
    }
    ваш код 
*/

function addition(n) {
    return function (x) {
        return console.log(n + x);
    };
}

const resultAddition = addition(4);

resultAddition(16);


/* 
    TASK 6 (Привязка контекста)
    
    Напишите функцию, которая будет выводить Имя и Фамилию юзера.
    Функция должна быть одна, а юзеров - пять. 
    Функция должна быть без параметров.

*/

const user1 = {
    firstName: 'Ivan',
    lastName: 'Ivanov'
};

const user2 = {
    firstName: 'Sidor',
    lastName: 'Sidorov'
};

const user3 = {
    firstName: 'Petr',
    lastName: 'Petrov'
};

const user4 = {
    firstName: 'Dmitriy',
    lastName: 'Dmitriev'
};

const user5 = {
    firstName: 'Sergey',
    lastName: 'Sergeev'
};

function getFullName() {
    return console.log(this.firstName + ' ' + this.lastName);
}

getFullName.call(user1);
getFullName.call(user2);
getFullName.call(user3);
getFullName.call(user4);
getFullName.call(user5);



//Задачи со звездочкой (опционально):

/* 
    TASK 7
    Напишите функцию которая принимает на вход натуральное число n, и выводит все нечетные числа до 0.
    Задачу необходимо решить с помощью рекурсии

    Example: oddNumbers(7) => 7,5,3,1
*/

function getNums(n) {
    if (n > 0 && n % 2 != 0) {
        console.log(n);
        getNums(n - 2);
    } else if (n % 2 == 0) {
        getNums(n - 1);
    }
}

getNums(3);

/* 
    TASK 8
    Необходимо найти факториал числа 5 с помощью рекурсии
*/

let i = 1;

function getFactorial(n) {
    i *= n;
    if (n > 1) {
        return getFactorial(n - 1);
    }

    return i;
}

let factorial = getFactorial(5);

console.log(factorial);

/* 
    TASK 9
    Напишите функцию customSetInterval(funcToExecute, interval)
    с помощью рекурсии и setTimeout,
    которая будет повторять функционал встроенного метода setInterval
    встроенный метод setInterval принимает два аргумента:
    первый аргумент - это функция, которая выполняется через заданный интервал времени
    второй аргумент - это временной интервал в миллисекундах.

    функция для вызова:
    function executeMe() {
        console.log('123')
    }

    пример вызова вашей функции:  
    customSetInterval(executeMe, 1000)
    в результате в консоли каждую секунду будет выводиться строка 123
*/

function customSetInterval(funcToExecute, interval) {
    setTimeout(function() {
        funcToExecute();
        return customSetInterval(funcToExecute,interval);
    },interval);
}

function executeMe() {
    console.log('123')
}

customSetInterval(executeMe, 1000);